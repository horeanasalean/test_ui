import { Component } from '@angular/core';
import { MessageService } from 'sdk/dist/out-tsc/app/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [MessageService],
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
}
